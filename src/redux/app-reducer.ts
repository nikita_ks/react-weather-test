import {AppStateType, InferActionsTypes} from "./store";
import {CityType, ResultCodesEnum, WeatherObjType} from './types'
import {ThunkAction} from "redux-thunk";
import {commonAsyncHandler} from "../dal/common-async-handler";
import {weatherAPI} from "../dal/weather-api";

export enum RequestStatus {
    NotInit = 0,
    Loading = 1,
    Error = 2,
    Success = 3
}

const initState = {
    isInitialized: false,
    requestStatus: RequestStatus.NotInit,
    errorMessage: '',
    cities: [
        {id: 0, city: 'Kyiv', country: 'Ukraine'},
        {id: 1, city: 'Minsk', country: 'Belarus'},
        {id: 2, city: 'Kremenchug', country: 'Ukraine'},
        {id: 3, city: 'Odessa', country: 'Ukraine'},
        {id: 4, city: 'Poznan', country: 'Poland'},
    ],
    weather: null as null | WeatherObjType,
    favoriteCities: [] as Array<CityType>,
    currentCity: null as null | CityType
};

type StateType = typeof initState
type ActionsTypes = InferActionsTypes<typeof actions>
type ThunkActionType = ThunkAction<Promise<void>, AppStateType, unknown, ActionsTypes>

const AppReducer = (state = initState, action: ActionsTypes): StateType => {
    switch (action.type) {

        case "APP_REDUCER/SET_IS_INIT":
            return {...state, isInitialized: true}

        case "APP_REDUCER/SET_REQUEST_STATUS":
            return {...state, requestStatus: action.status}

        case "APP_REDUCER/SET_ERROR_MESSAGE":
            return {...state, errorMessage: action.errorMessage}

        case "APP_REDUCER/SET_WEATHER":
            return {...state, weather: action.weatherObj}

        case "APP_REDUCER/SET_CURRENT_CITY":
            return {...state, currentCity: action.city}

        case "APP_REDUCER/SET_FAVORITE_CITY": {
            const city = state.favoriteCities.find((c) => c.id === action.city.id)
            if (!city) {
                return {...state, favoriteCities: [...state.favoriteCities, action.city]}
            } else {
                return state
            }
        }
        case "APP_REDUCER/DELETE_FAVORITE_CITY":
            return {...state, favoriteCities: state.favoriteCities.filter(c => c.id !== action.city.id)}
        default:
            return state
    }
}

export const actions = {
    setIsInit: () => ({type: 'APP_REDUCER/SET_IS_INIT'}) as const,
    setRequestStatus: (status: RequestStatus) => ({type: 'APP_REDUCER/SET_REQUEST_STATUS', status}) as const,
    setErrorMessage: (errorMessage: string) => ({type: 'APP_REDUCER/SET_ERROR_MESSAGE', errorMessage}) as const,
    setWeather: (weatherObj: WeatherObjType) => ({type: 'APP_REDUCER/SET_WEATHER', weatherObj}) as const,
    setCurrentCity: (city: CityType) => ({type: 'APP_REDUCER/SET_CURRENT_CITY', city}) as const,
    setFavoriteCity: (city: CityType) => ({type: 'APP_REDUCER/SET_FAVORITE_CITY', city,}) as const,
    deleteFavoriteCity: (city: CityType) => ({type: 'APP_REDUCER/DELETE_FAVORITE_CITY', city,}) as const
}

export const isInit = (): ThunkActionType => async (dispatch) => {
}

export const getWeather = (cityName: string): ThunkActionType => async (dispatch, getState) => {
    await commonAsyncHandler(async () => {
        const response = await weatherAPI.getWeatherByCityName(cityName)
        dispatch(actions.setWeather(response))
        const currentCity = getState().app.cities.find((c) => c.city === cityName)
        if (currentCity) {
            dispatch(actions.setCurrentCity(currentCity))
            dispatch(actions.setFavoriteCity(currentCity))
        }
        return response
    }, dispatch);
}

export default AppReducer
