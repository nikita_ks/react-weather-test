export type ResponseType<T> = {
    resultCode: ResultCodesEnum
    message: string
    accessToken: string
    data: T

}

export enum ResultCodesEnum {
    Success = 0,
    Error = 1
}

export type CityType = {
    id: number
    city: string
    country: string
}


export type ResponseWeather = {
    "coord": {
        "lon": number,
        "lat": number
    },
    "weather": [
        {
            "id": number,
            "main": string,
            "description": string,
            "icon": string
        }
    ],
    "base": string,
    "main": {
        "temp": number,
        "feels_like": number,
        "temp_min": number,
        "temp_max": number,
        "pressure": number,
        "humidity": number
    },
    "visibility": number,
    "wind": {
        "speed": number,
        "deg": number
    },
    "clouds": {
        "all": number
    },
    "dt": number,
    "sys": {
        "type": number,
        "id": number,
        "country": string,
        "sunrise": number,
        "sunset": number
    },
    "timezone": number,
    "id": number,
    "name": string,
    "cod": number
}

export type WeatherObjType = {
    temp: number
    feelTemp: number
}