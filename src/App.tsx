import React from 'react';
import './App.css'
import SearchContainer from "./components/search-component/SearchContainer";

const App = () => {
    return (
        <div className="App">
            <SearchContainer/>
        </div>
    )
};

export default App;
