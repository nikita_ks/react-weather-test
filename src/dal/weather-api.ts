import axios from 'axios';
import {ResponseWeather} from "../redux/types";

const instance = axios.create({
    baseURL: 'https://api.openweathermap.org/data/2.5/'
})
const apiKey = '361b7a0701aaa71c85316e85b760c7df';
export const weatherAPI = {
    async getWeatherByCityName(cityName: string) {
        return instance.get<ResponseWeather>(`weather?q=${cityName}&appid=${apiKey}`)
            .then((res) => (
                {
                    temp: Math.ceil(res.data.main.temp - 273.15),
                    feelTemp: Math.ceil(res.data.main.feels_like - 273.15)
                }
            ))
    }
}
