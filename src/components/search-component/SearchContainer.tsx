import React, {ChangeEvent, FC, useState} from 'react';
import SearchComponent from "./SearchComponent";
import {useDispatch, useSelector} from "react-redux";
import {AppStateType} from "../../redux/store";
import {searchSelector} from '../../helpers/selectors';
import {actions, getWeather} from "../../redux/app-reducer";
import {CityType} from "../../redux/types";

const SearchContainer: FC = () => {
    const [value, setValue] = useState('')
    const dispatch = useDispatch()
    const cities = useSelector((state: AppStateType) => searchSelector(state, value))
    const {weather, favoriteCities, currentCity} = useSelector((state: AppStateType) => state.app)

    const onInputChange = (e: ChangeEvent<HTMLInputElement>) => {
        setValue(e.currentTarget.value)
    }
    const getWeatherData = () => {
        if (!!value && !!cities.length) {
            dispatch(getWeather(value))
        }
    }
    const onCitySelect = (value: string) => {
        setValue(value)
        dispatch(getWeather(value))
        setValue('')
    }
    const deleteCity = (city: CityType) => {
        dispatch(actions.deleteFavoriteCity(city))
    }
    return (
        <div className='search-component'>
            <div className="container">
                <SearchComponent weather={weather} getWeatherData={getWeatherData} cities={cities}
                                 onInputChange={onInputChange} currentCity={currentCity}
                                 favoriteCities={favoriteCities} deleteCity={deleteCity}
                                 value={value} handelClick={onCitySelect}/>
            </div>
        </div>
    );
}

export default SearchContainer;