import React, {ChangeEvent, FC, useState} from 'react';
import {CityType, WeatherObjType} from '../../redux/types';
import OptionComponent from '../option-component/OptionComponent';
import searchIcon from '../../assets/search.svg';

type PropsType = {
    cities: Array<CityType>
    onInputChange: (e: ChangeEvent<HTMLInputElement>) => void
    value: string
    getWeatherData: () => void
    handelClick: (city: string) => void
    weather: WeatherObjType | null
    favoriteCities: Array<CityType>
    currentCity: CityType | null
    deleteCity: (city: CityType) => void
}

const SearchComponent: FC<PropsType> = (props) => {
    const {
        cities, weather, onInputChange, deleteCity,
        favoriteCities, value, getWeatherData, handelClick
    } = props
    const [showFavorite, setShowFavorite] = useState(false)

    const getWeatherHandler = (city: string) => {
        handelClick(city)
        setShowFavorite(false)
    }
    const handlerOutBlock = (e: any) => {
        if (e.target.className === 'search-in') {
            setShowFavorite(false)
        }
    }
    const mappedFavoriteCities = favoriteCities.map((c) => {
        return <OptionComponent showCross={true} key={c.id} cityData={c}
                                onClick={getWeatherHandler} deleteCity={deleteCity}/>
    })

    const mappedCities = cities.map((c) => {
        return <OptionComponent showCross={false} onClick={getWeatherHandler} cityData={c} key={c.id}/>
    })

    return (
        <div className='search-in' onClick={handlerOutBlock}>
            <div className="search-block">
                <input value={value} onFocus={() => setShowFavorite(true)}
                       onChange={onInputChange} type="text" placeholder='City name'/>
                <div className='search-icon' onClick={getWeatherData}>
                    <img src={searchIcon} alt="search"/>
                </div>
                {
                    showFavorite && !!favoriteCities.length &&
                    <div className="favorite-city-block">
                        {mappedFavoriteCities}
                    </div>
                }
                {
                    value && showFavorite &&
                    <div className='cities-block'>
                        {mappedCities}
                    </div>
                }
                {
                    weather && props.currentCity &&
                    <div className="weather-info">
                        <div>City: {props.currentCity.city}</div>
                        <div>
                            Temperature: {weather.temp} &deg;C
                        </div>
                        <div>
                            Feels like: {weather.feelTemp} &deg;C
                        </div>
                    </div>
                }
            </div>
        </div>
    );
}

export default SearchComponent;