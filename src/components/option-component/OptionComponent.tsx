import React, {FC} from 'react';
import {CityType} from "../../redux/types";

type PropsType = {
    onClick: (city: string) => void
    cityData: CityType
    showCross: boolean
    deleteCity?: (city: CityType) => void
}

const OptionComponent: FC<PropsType> = (props) => {
    const deleteFavoriteCity = (e: React.MouseEvent<HTMLDivElement>) => {
        e.stopPropagation()
        props.deleteCity && props.deleteCity(props.cityData)
    }
    return (
        <div className="option-wrapper" onClick={() => props.onClick(props.cityData.city)}>
            <div className="body">
                <span>{props.cityData.city},{props.cityData.country}</span>
            </div>
            {
                props.showCross &&
                <div className="close-icon" onClick={deleteFavoriteCity}/>
            }
        </div>
    );
}

export default OptionComponent;