import {AppStateType} from "../redux/store";


export const searchSelector = (state: AppStateType, searchStr: string) => {
    const {cities} = state.app
    return cities.filter((city) => {
        return city.city.toLowerCase().indexOf(searchStr.toLowerCase()) > -1
    })
}